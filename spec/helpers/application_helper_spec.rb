# frozen_string_literal: true

require "rails_helper"

RSpec.describe "ApplicationHelper", type: :helper do
  describe "Application Title helpers" do
    include ApplicationHelper
    it { expect(full_title("test")).to eq "test - BIGBAG Store" }
    it { expect(full_title("")).to eq "BIGBAG Store" }
    it { expect(full_title(nil)).to eq "BIGBAG Store" }
  end
end
