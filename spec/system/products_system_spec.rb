require "rails_helper"

RSpec.describe "Products_system", type: :system do
  let(:category) { create(:taxonomy, name: "Category") }
  let!(:taxon) { create(:taxon, name: "Taxon", taxonomy: category, parent: category.root) }
  let!(:product) { create(:product, taxons: [taxon], name: "Product", price: "23.45") }
  let!(:related_products) { create_list(:product, 5, taxons: [taxon]) }
  let!(:other_product) { create(:product) }

  scenario "商品の表示について" do
    visit potepan_product_path(product.id)
    # 現在のパスが指定されたパスであるか
    expect(current_path).to eq potepan_product_path(product.id)
    # ページ内に指定の要素が表示されているか
    expect(page).to have_title "#{product.name} - BIGBAG Store"
    expect(page).to have_selector ".page-title h2", text: product.name
    expect(page).to have_selector ".media-body h2", text: product.name
    expect(page).to have_selector ".media-body h3", text: product.display_price
    expect(page).to have_selector ".media-body p", text: product.description
    expect(page).to have_link "Home", href: potepan_root_path
    expect(page).to have_link "一覧ページへ戻る", href: potepan_category_path(product.taxons.first.id)
  end

  scenario "関連商品の表示について" do
    visit potepan_product_path(product.id)
    # 関連商品表示されるか
    expect(page).to have_selector ".productCaption h5", text: related_products.first.name
    expect(page).to have_selector ".productCaption h3", text: related_products.first.display_price
    # 関連商品が5件以上ある場合でも４件の表示となる
    expect(page).to have_selector ".productBox", count: 4
    # 関連しない商品は表示されない
    expect(page).to have_no_content other_product.name
  end

  scenario "関連商品のリンクをテスト" do
    visit potepan_product_path(product.id)
    expect(page).to have_link related_products.first.name, href: potepan_product_path(related_products.first.id)
  end
end
