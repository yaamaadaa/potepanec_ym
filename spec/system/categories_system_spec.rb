require "rails_helper"

RSpec.describe "Categories_system", type: :system do
  let(:taxonomy) { create(:taxonomy, name: "category") }
  let!(:clothing) { create(:taxon, name: "clothing", taxonomy: taxonomy, parent: taxonomy.root) }
  let!(:bag) { create(:taxon, name: "bag", taxonomy: taxonomy, parent: taxonomy.root) }
  let!(:shirts) { create(:taxon, name: "shirts", taxonomy: taxonomy, parent: clothing) }
  let!(:product) { create(:product, name: "Ruby on Rails T-Shirt", price: "23.45", taxons: [shirts]) }
  let!(:other_product) { create(:product, name: "other-bag", price: "98.76", taxons: [bag]) }

  scenario "カテゴリーページの表示について" do
    visit potepan_category_path(taxonomy.root.id)
    # 現在のパスが指定されたパスであるか
    expect(current_path).to eq potepan_category_path(taxonomy.root.id)
    # ページ内に指定の要素が表示されているか
    expect(page).to have_title "#{taxonomy.root.name} - BIGBAG Store"
    expect(page).to have_selector ".page-title", text: taxonomy.name
    expect(page).not_to have_selector ".panel-body", text: clothing.name
    expect(page).to have_selector ".panel-body", text: bag.name
    expect(page).to have_selector ".panel-body", text: shirts.name
    expect(page).to have_selector ".productCaption h5", text: product.name
    expect(page).to have_selector ".productCaption h3", text: product.display_price
  end

  scenario "各カテゴリ商品の表示について" do
    visit potepan_category_path(bag.id)
    # 現在のパスが指定されたパスであるか
    expect(current_path).to eq potepan_category_path(bag.id)
    # ページ内に指定の要素が表示されているか
    expect(page).to have_title "#{bag.name} - BIGBAG Store"
    expect(page).to have_selector ".page-title h2", text: "bag"
    expect(page).not_to have_selector ".productCaption h5", text: product.name
    expect(page).to have_selector ".productCaption h5", text: other_product.name
    expect(page).not_to have_selector ".productCaption h3", text: product.display_price
    expect(page).to have_selector ".productCaption h3", text: other_product.display_price
  end

  scenario "カテゴリへのリンクをテスト" do
    visit potepan_category_path(taxonomy.root.id)
    expect(page).to have_link bag.name, href: potepan_category_path(bag.id)
    expect(page).to have_link shirts.name, href: potepan_category_path(shirts.id)
  end

  scenario "商品詳細へのリンクをテスト" do
    visit potepan_category_path(taxonomy.root.id)
    expect(page).to have_link product.name, href: potepan_product_path(product.id)
  end
end
