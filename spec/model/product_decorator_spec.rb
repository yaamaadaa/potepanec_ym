require "rails_helper"

RSpec.describe Spree::Product, type: :model do
  describe "products_decorator" do
    let(:taxon) { create_list(:taxon, 4) }
    let(:product_1) { create(:product, taxons: [taxon[0], taxon[1], taxon[2]]) }
    let(:product_2) { create(:product, taxons: [taxon[0]]) }
    let(:product_3) { create(:product, taxons: [taxon[1], taxon[2]]) }
    let(:product_4) { create(:product, taxons: [taxon[3]]) }

    it "関連した商品を取得する" do
      expect(product_1.related_products).to contain_exactly(product_2, product_3)
    end

    context "関連するProductが存在しない" do
      it "商品を取得しない" do
        expect(product_4.related_products).to eq []
      end
    end
  end
end
