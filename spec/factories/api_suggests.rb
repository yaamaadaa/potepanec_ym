FactoryBot.define do
  factory :suggest, class: "Potepan::Suggest" do
    keyword { "ruby" }
  end
end
