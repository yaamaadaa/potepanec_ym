require "rails_helper"

RSpec.describe "Products_request", type: :request do
  describe "GET products#show" do
    let(:category) { create(:taxonomy, name: "Category") }
    let(:taxon) { create(:taxon, name: "Taxon", taxonomy: category, parent: category.root) }
    let!(:product) { create(:product, taxons: [taxon], name: "Product", price: "23.45") }
    let!(:related_products) { create(:product, name: "related_product", price: "19.99", taxons: [taxon]) }

    before do
      get potepan_product_url(product.id)
    end

    it "正常なレスポンスか?" do
      expect(response).to be_successful
    end

    it "200レスポンスが返ってくるか" do
      expect(response).to have_http_status 200
    end

    it "showテンプレートで表示されること" do
      expect(response).to render_template :show
    end

    it "商品が表示されていることを確認" do
      expect(response.body).to include product.name
      expect(response.body).to include product.display_price.to_s
      expect(response.body).to include product.description
      expect(response.body).to include related_products.name
      expect(response.body).to include related_products.display_price.to_s
    end
  end
end
