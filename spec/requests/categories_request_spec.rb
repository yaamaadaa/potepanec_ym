require "rails_helper"

RSpec.describe "Products_request", type: :request do
  describe "GET categories#show" do
    let(:category) { create(:taxonomy, name: "Category") }
    let(:bag) { create(:taxon, name: "Bag", taxonomy: category) }
    let(:mug) { create(:taxon, name: "Mug", taxonomy: category) }
    let!(:rails_bag) { create(:product, taxons: [bag], name: "Ruby on Rails Bag") }
    let!(:rails_mug) { create(:product, taxons: [mug], name: "Ruby on Rails Mug") }

    before do
      get potepan_category_path(bag.id)
    end

    it "正常なレスポンスか?" do
      expect(response).to be_successful
    end

    it "200レスポンスが返ってくるか" do
      expect(response).to have_http_status 200
    end

    it "showテンプレートで表示されること" do
      expect(response).to render_template :show
    end

    it "商品が表示されていることを確認" do
      expect(response.body).to include rails_bag.name
      expect(response.body).to include rails_bag.display_price.to_s
    end

    it "指定したカテゴリーの商品のみが表示されていることを確認" do
      expect(response.body).not_to include rails_mug.name
    end
  end
end
