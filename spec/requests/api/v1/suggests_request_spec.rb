require "rails_helper"

describe Potepan::Api::V1::SuggestsController, type: :request do
  describe "GET /api/v1/suggest" do
    let!(:keyword1) { create(:suggest, keyword: "ruby") }
    let!(:keyword2) { create(:suggest, keyword: "abbington") }
    let!(:keyword3) { create(:suggest, keyword: "abbot") }
    let!(:keyword4) { create(:suggest, keyword: "abel") }
    let(:headers) { { "Authorization" => "Bearer #{Rails.application.credentials.myapi[:suggest_myapi_key]}" } }

    context "リクエストが成功した場合" do
      before do
        get potepan_api_v1_suggests_path, params: { keyword: "ab", max_num: 2 }, headers: headers
      end

      it "200レスポンスが返ってくる" do
        expect(response.status).to eq(200)
      end

      it "検索候補をmax_numの数だけ取得できる" do
        json = JSON.parse(response.body)
        expect(json.size).to eq 2
        expect(json[0]).to eq keyword2[:keyword]
        expect(json[1]).to eq keyword3[:keyword]
      end

      it "検索マッチしないキーワードは返さない" do
        json = JSON.parse(response.body)
        expect(json).not_to include eq keyword1[:keyword]
      end
    end

    context "keywordが存在しない場合" do
      it "422レスポンスが返ってくる" do
        get potepan_api_v1_suggests_path, headers: headers
        expect(response.status).to eq(422)
      end
    end

    context "認証キーを間違えた場合" do
      it "401レスポンスが返ってくる" do
        get potepan_api_v1_suggests_path, headers: { "Authorization" => "Bearer misspass" }
        expect(response.status).to eq(401)
      end
    end
  end
end
