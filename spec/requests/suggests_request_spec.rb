require "rails_helper"

RSpec.describe "Suggests_request", type: :request do
  describe "GET suggests#search" do
    url = Rails.application.credentials.potepan_suggest[:suggest_url]
    key = Rails.application.credentials.potepan_suggest[:api_key]

    context "keywordを入力" do
      before do
        WebMock.enable!
        stub_request(:get, url).
          with(
            headers: { "Authorization" => "Bearer #{key}" },
            query: { keyword: "ab", max_num: "5" },
          ).
          to_return(
            body: ["abbey", "abbington", "abbot", "abel", "abercrombie"].to_json,
            status: 200,
            headers: { "Content-Type" => "application/json" },
          )
      end

      it "200レスポンスが返ってくるか" do
        get potepan_suggests_path, params: { keyword: "ab" }
        expect(response).to have_http_status 200
      end

      it "レスポンスにabを含んだ候補を5個返すか" do
        get potepan_suggests_path, params: { keyword: "ab" }
        expect(JSON.parse(response.body)).to eq ["abbey", "abbington", "abbot", "abel", "abercrombie"]
      end
    end

    context "keywordが空欄" do
      it "400レスポンスが返ってくるか" do
        get potepan_suggests_path, params: { keyword: nil }
        expect(response).to have_http_status 400
      end
    end
  end
end
