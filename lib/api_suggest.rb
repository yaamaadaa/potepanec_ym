require "httpclient"

class ApiSuggest
  API_KEY = Rails.application.credentials.potepan_suggest[:api_key]
  API_URL = Rails.application.credentials.potepan_suggest[:suggest_url]

  def self.suggest(keyword, max_num)
    client = HTTPClient.new
    headers = {
      "Authorization" => "Bearer #{API_KEY}",
    }
    query = {
      "keyword" => keyword,
      "max_num" => max_num,
    }
    client.get(API_URL, query, headers)
  end
end
