class Potepan::Api::ApiController < ApplicationController
  rescue_from StandardError, with: :rescue_500

  # rubocop:disable Layout/IndentationWidth
  protected

  def rescue_500(e)
    logger.error "Rendering 500 with exception: #{e.message}"
    render json: { "error": { "code": 500, "message": "内部サーバーエラー" } }, status: 500
  end

  def authenticate
    render_unauthorized unless authenticate_token
  end

  def authenticate_token
    authenticate_with_http_token do |token, _options|
      myapi_key = Rails.application.credentials.myapi[:suggest_myapi_key]
      ActiveSupport::SecurityUtils.secure_compare(token, myapi_key)
    end
  end

  def render_unauthorized
    render json: { "error": { "code": 401, "message": "認証エラー" } }, status: 401
  end
end
# rubocop:enable Layout/IndentationWidth
