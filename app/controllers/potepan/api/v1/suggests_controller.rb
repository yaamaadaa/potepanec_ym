class Potepan::Api::V1::SuggestsController < Potepan::Api::ApiController
  include ActionController::HttpAuthentication::Token::ControllerMethods

  before_action :authenticate

  def index
    render json: { "error": { "code": 422, "message": "keywordがありません" } }, status: 422 and return if params[:keyword].blank?

    suggests = Potepan::Suggest.search_by_keyword(params[:keyword]).limit(params[:max_num]).pluck(:keyword)
    render json: suggests
  end
end
