require "api_suggest"

class Potepan::SuggestsController < ApplicationController
  def index
    if params[:keyword] && params[:keyword].length <= KEYWORD_LENGTH_LIMIT_COUNT
      response = ApiSuggest.suggest(params[:keyword], SUGGEST_MAX_COUNT)
      render json: JSON.parse(response.body), status: response.status
    else
      render json: [], status: 400
    end
  end
end
