class Potepan::ProductsController < ApplicationController
  def show
    @product = Spree::Product.find(params[:id])
    @related_products = @product.related_products.
                          includes(master: [:default_price, :images]).sample(MAX_DISPLAY_RELATED_PRODUCTS_COUNT)
  end
end
